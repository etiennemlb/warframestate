
/***********************************************************
 * Config :
 *
 */
const tickPerMinutes = 6;
const tickRateMs = 60000 / tickPerMinutes;
const apiPath = 'api.php';
const ressourcesPath = '';

/***********************************************************
 * DATA
 */

let currentWorldState = undefined;
let nodeMap = undefined;
let missionsTypeMap = undefined;

/***********************************************************
 * Tools
 */

const makeRequest = function (method = 'GET', url, async = 'false') {
    let xhr = new XMLHttpRequest();

    if ('withCredentials' in xhr) {
        xhr.open(method, url, async);
    } else if (typeof XDomainRequest != 'undefined') {
        xhr = new XDomainRequest();
        xhr.open(method, url, async);
    } else {
        xhr = null;
    }

    if (!xhr) throw new Error('CORS not supported');

    return xhr;
}

const tsToDate = function (timeStamp) {
    return new Date(parseInt(timeStamp));
}

const msToSecMinHourDay = function (mills) {
    secondsFromMills = Math.floor(mills * 0.001);

    return {
        seconds: secondsFromMills % 60,
        minutes: Math.floor(secondsFromMills * 0.016666667) % 60,
        hours: Math.floor(secondsFromMills * 0.000277778) %
            24,  // a / 60 / 60 = a / (60^2) = a * 0,000277778
        days: Math.floor(secondsFromMills * 0.000011574)  // 1 / (1 * 60 * 60 * 24)
    };
}

/**
 * TODO reverse map
 * @param {*} stringID 
 */
const nodeAndPlanetNameFromId = function (stringID) {
    let planetName = '?PLANET?';
    let nodeName = '?NODE?';

    stringID = stringID.toUpperCase();

    for (let i = 0; i < nodeMap.nodes.length; ++i) {
        let node = nodeMap.nodes[i];

        if (node.node_id.toUpperCase() === stringID) {
            planetName = node.planet_id;
            nodeName = node.name;
            break;
        }
    }

    for (let i = 0; i < nodeMap.planets.length; ++i) {
        let planet = nodeMap.planets[i];

        if (planet.planet_id == planetName) {
            planetName = planet.name;
            break;
        }
    }

    return { 'planetName': planetName, 'nodeName': nodeName };
}

/**
 * TODO reverse map
 * @param {*} missionID 
 */
const missionTypeFromMTID = function (missionID) {
    for (let i = 0; i < missionsTypeMap.length; ++i) {
        let mt = missionsTypeMap[i];

        if (mt.id == missionID) {
            return mt.type;
        }
    }

    return '?MISSION?';
}

const factionFromFactionID = function (factionID) {
    switch (factionID) {
        case 'FC_INFESTATION':
            return 'Infestation';
        case 'FC_CORPUS':
            return 'Corpus';
        case 'FC_GRINEER':
            return 'Grineer';
        case 'FC_OROKIN':
            return 'Corrupted';
        default:
            return '?FACTION?';
    }
}
/***********************************************************/

/***
 * CSS selector
 */
const selector = {
    'news': '#newsframe',
    'voidfissures': '#voidfissuresframe',
    'invasions': '#invasionsframe',
    'dailydeals': '#dailydealsframe',
    'voidtraders': '#voidtraderframe',
    'alerts': '#alertsframe',
    'sorties': '#sortiesframe',
    'cetuspoetime': '#cetusdaynight-timeleft',
    'cetuspoedaynightcycle': '#cetusdaynight'
};

const updateNews = function () {
    $(selector.news).empty();

    for (let j = currentWorldState.Events.length - 1; j >= 0; --j) {
        let event = currentWorldState.Events[j];

        let messageText = '';
        let englishFound = false;

        for (let i = 0; i < event.Messages.length; ++i) {
            if (event.Messages[i].LanguageCode === 'en') {
                // console.log(event.Messages[i].Message);
                messageText = event.Messages[i].Message;
                englishFound = true;
                break;
            }
        }

        if (!englishFound) continue;

        let timeElapsed =
            msToSecMinHourDay(new Date() - tsToDate(event.Date.$date.$numberLong));
        let url = event.Prop;

        $(selector.news).append(`<div>[${timeElapsed.days}d] 
                    <a href="${url} target="_blank">${messageText}</a>
            </div>
            `);
    }
}

const updateVoidFissures = function () {
    $(selector.voidfissures).empty();

    for (let j = currentWorldState.ActiveMissions.length - 1; j >= 0; --j) {
        let voidFissure = currentWorldState.ActiveMissions[j];

        let node = nodeAndPlanetNameFromId(voidFissure.Node);
        let missionType = missionTypeFromMTID(voidFissure.MissionType);
        let relicTier = voidFissure.Modifier;
        let expireIn = msToSecMinHourDay(
            tsToDate(voidFissure.Expiry.$date.$numberLong) - new Date());

        $(selector.voidfissures)
            .append(`<li class="list-group-item fissureitem">
                <span class="badge time" style="background-color: green;">
                    <span title="">${
                expireIn.hours + 'h ' + expireIn.minutes + 'm ' + expireIn.seconds +
                's'}</span>
                </span>
                <span class="bold">${node.nodeName} (${node.planetName}) (${
                missionType})</span> | 
                <span>${relicTier}</span>
            </li>
            `);
    }
}

/**
 * TODO
 * Map reward id to reward name
 */
const updateInvasions = function () {
    $(selector.invasions).empty();

    for (let j = currentWorldState.Invasions.length - 1; j >= 0; --j) {
        let invasion = currentWorldState.Invasions[j];

        if (invasion.Completed) continue;

        let node = nodeAndPlanetNameFromId(invasion.Node);
        let invadingFC = factionFromFactionID(invasion.AttackerMissionInfo.faction);
        let defendingFC =
            factionFromFactionID(invasion.DefenderMissionInfo.faction);
        let invadingReward = invasion.AttackerReward.countedItems;
        let defendingReward = invasion.DefenderReward.countedItems;
        let invadingPercentage =
            (Math.abs((invasion.Goal + invasion.Count) / (invasion.Goal * 2)) *
                100);

        let htmlCODE = `<div class="invasion-entry">
            <table class="invasion-table">
                <tr>
                    <td colspan="2">
                        <span class="invasion-node">${
            node.nodeName}</span> (<span class="invasion-region">${
            node.planetName}</span>)
                        - <span class="invasion-desc"></span>
                        <span class="">${defendingFC}</span>
                        vs
                        <span class="">${invadingFC}</span> -
                        <span class="invasion-percent" title="">${
            invadingPercentage.toFixed(1).toString()}%</span>
                    </td>
                </tr>        
                <tr>
                <td valign="bottom" class="invading-cell">`;

        // defending reward
        htmlCODE += ``;

        if (invadingReward != undefined) {
            invadingReward.forEach(reward => {
                let rewardName = reward.ItemType.split('/').pop();

                htmlCODE +=
                    `<span class="badge" style="float: left; background-color: #1d47d1;">${
                    reward.ItemCount + ' ' + rewardName}</span>`;
            });
        }

        htmlCODE += `</td>`;

        // invading reward
        htmlCODE += `<td valign="bottom" class="defending-cell">`;

        if (defendingReward != undefined) {
            defendingReward.forEach(reward => {
                let rewardName = reward.ItemType.split('/').pop();

                htmlCODE +=
                    `<span class="badge" style="float: right; background-color: #1d47d1;">${
                    reward.ItemCount + ' ' + rewardName}</span>`;
            });
        }

        htmlCODE += `</td>
            </tr>       
            </table>        
                <div class="progress">
                    <div class="progress-bar ${defendingFC}" style="width:${
            invadingPercentage}%">
                        <span class="faction-bar"><img src="img/${
            defendingFC.toLowerCase()}.png" style="height:20px;float:left;"></span>
                    </div>
                    <div class="progress-bar arrow-left ${
            invadingFC}" style="width:${100 - invadingPercentage}%">
                        <span class="faction-bar"><img src="img/${
            invadingFC.toLowerCase()}.png" style="height:20px;float:right;"></span>
                    </div>
                </div>
            </div>`;

        $(selector.invasions).append(htmlCODE);
    }
}

const updateDarvoDeals = function () {
    $(selector.dailydeals).empty();

    for (let j = currentWorldState.DailyDeals.length - 1; j >= 0; --j) {
        let deal = currentWorldState.DailyDeals[j];

        let timeLeft =
            msToSecMinHourDay(tsToDate(deal.Expiry.$date.$numberLong) - new Date());
        let itemName = deal.StoreItem.split('/').pop();
        let discount = deal.Discount;
        let platePrice = deal.SalePrice;
        let itemLeft = deal.AmountTotal - deal.AmountSold;
        let totalItem = deal.AmountTotal;

        $(selector.dailydeals)
            .append(`<li class="list-group-item darvodaily">
                <span class="badge time" style="background-color: green;">
                    <span>${
                timeLeft.hours + 'h ' + timeLeft.minutes + 'm ' + timeLeft.seconds +
                's'}</span>
                </span>
                <span class="deal-name bold">${itemName}</span> |
                <span class="deal-discount">${discount}% off</span> |
                <span class="deal-amount bold">
                    <img src="img/plat.png" style="height:16px" title="platinum" alt="platinum"> ${
                platePrice}</span> |
                <span class="deal-inventory">
                    <strong>${itemLeft}</strong>/${totalItem} left</span>
            </li>`);
    }
}

const updateVoidTrader = function () {
    $(selector.voidtraders).empty();

    for (let j = currentWorldState.VoidTraders.length - 1; j >= 0; --j) {
        let trader = currentWorldState.VoidTraders[j];

        let traderName = trader.Character;
        let node = trader.Node;
        let time = undefined;
        let itemsSold = trader.Manifest;

        if (trader.Manifest == undefined) {
            time = msToSecMinHourDay(
                tsToDate(trader.Activation.$date.$numberLong) - new Date());
        } else {
            time = msToSecMinHourDay(
                tsToDate(trader.Expiry.$date.$numberLong) - new Date());
        }

        let htmlCODE = `<span style="text-align:left;">
                <span>
                    <strong>${traderName}</strong> - ${node}
                </span>
                <span class="pull-right">
                    | <span>Time left: <strong id="voidtraderstimeleft">${
            time.days + 'd ' + time.hours + 'h ' + time.minutes + 'm ' +
            time.seconds + 's'}</strong></span>
                </span>
                <br style="clear: both;">
            </span>

            <div class="well">
                <table class="table table-bordered table-condensed table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Ducats</th>
                            <th>Credits</th>
                        </tr>
                    </thead>
                    <tbody>`;

        if (itemsSold != undefined) {
            itemsSold.forEach(item => {
                let itemName = item.ItemType.split('/').pop();
                let ducats = item.PrimePrice;
                let credits = item.RegularPrice;

                htmlCODE += `<tr>
                        <td>${itemName}</td>
                        <td>${ducats}</td>
                        <td>${credits}</td>
                    </tr>`;
            });
        }

        htmlCODE += `
                    </tbody>
                </table>
            </div>`;

        $(selector.voidtraders).append(htmlCODE);

        $('#voidtraderstimeleft').css('color', 'black');
        if (itemsSold != undefined) $('#voidtraderstimeleft').css('color', 'green');
    }
}

const updateAlerts = function () {
    $(selector.alerts).empty();

    for (let j = currentWorldState.Alerts.length - 1; j >= 0; --j) {
        let alert = currentWorldState.Alerts[j];

        let timeLeft = tsToDate(alert.Expiry.$date.$numberLong) - new Date();
        if (timeLeft < 0) continue;
        timeLeft = msToSecMinHourDay(
            tsToDate(alert.Expiry.$date.$numberLong) - new Date());

        let creditReward = alert.MissionInfo.missionReward.credits;

        let countedItemsRewards = alert.MissionInfo.missionReward.countedItems;
        let itemsReward = alert.MissionInfo.missionReward.items;

        let node = nodeAndPlanetNameFromId(alert.MissionInfo.location);
        let missionType = missionTypeFromMTID(alert.MissionInfo.missionType);
        let faction = factionFromFactionID(alert.MissionInfo.faction);
        let minLevel = alert.MissionInfo.minEnemyLevel;
        let maxLevel = alert.MissionInfo.maxEnemyLevel;

        let htmlCODE = `<li class="list-group-item">
                    <p class="alert-badge">
                        <span class="badge time alert2" style="background-color: green;">
                            <span>${
            (new Date() - tsToDate(alert.Activation.$date.$numberLong) < 0) ?
                ('Starting soon') :
                (timeLeft.hours + 'h ' + timeLeft.minutes + 'm ' +
                    timeLeft.seconds + 's')}</span>
                        </span>
                    </p>
                    <span class="badge alert2">${creditReward}cr</span>`;


        if (countedItemsRewards != undefined) {
            countedItemsRewards.forEach(reward => {
                let rewardName = reward.ItemType.split('/').pop();

                switch (rewardName) {
                    // mdr alertium
                    case 'Alertium':
                        rewardName = 'Nitain Extract';
                        break;

                    case 'VoidTearDrop':
                        rewardName = 'Void Traces';

                    default:
                        break;
                }

                htmlCODE +=
                    `<span class="badge alert2" style="background-color:#0070dd;">${
                    reward.ItemCount + ' ' + rewardName}</span>`;
            });
        }

        if (itemsReward != undefined) {
            itemsReward.forEach(item => {
                let itemName = item.split('/').pop();

                switch (itemName) {
                    case 'AlertFusionBundleLarge':
                        itemName = '150 endo';
                        break;

                    case 'AlertFusionBundleMedium':
                        itemName = '100 endo';
                        break;

                    case 'AlertFusionBundleSmall':
                        itemName = '80 endo';
                        break;

                    default:
                        break;
                }

                htmlCODE +=
                    `<span class="badge alert2" style="background-color:#a335ee;">${
                    itemName}</span>`;
            });
        }

        htmlCODE += `
                    <span class="alert-node">${node.nodeName} (${
            node.planetName})</span> | <span class="alert-type">${
            missionType}</span>
                    (<span class="alert-fc">${faction}</span>) | Level: ${
            minLevel}-${maxLevel}
            </li>`;

        $(selector.alerts).append(htmlCODE);
    }
}

const updateSorties = function () {
    $(selector.sorties).empty();

    for (let j = currentWorldState.Sorties.length - 1; j >= 0; --j) {
        let sortie = currentWorldState.Sorties[j];

        let timeLeft = msToSecMinHourDay(tsToDate(sortie.Expiry.$date.$numberLong) - new Date());

        for (let i = 0; i < sortie.Variants.length; ++i) {
            let mission = sortie.Variants[i];

            let node = nodeAndPlanetNameFromId(mission.node);
            let missionType = missionTypeFromMTID(mission.missionType);
            let condition = mission.modifierType;

            $(selector.sorties)
                .append(`<li class="list-group-item sortievariant">
                    <span class="badge sortietime" style="background-color: green;">
                        <span>${
                    timeLeft.hours + 'h ' + timeLeft.minutes + 'm'}</span>
                    </span>
                    <span class="bold">${node.nodeName} (${
                    node.planetName})</span> | 
                    <span>${missionType}</span> | 
                    <span>${condition}</span>
                </li>`);
        }
    }
}

const updateCetusPOE = function () {
    $(selector.cetuspoetime).empty();
    $(selector.cetuspoedaynightcycle).empty();

    let cetus = undefined;

    for (let j = 0; j < currentWorldState.SyndicateMissions.length; ++j) {
        let mission = currentWorldState.SyndicateMissions[j];

        if (mission.Tag === 'CetusSyndicate') {
            cetus = mission;
            break;
        }
    }
    if (cetus === undefined) return;


    let timeLeftUntilNewCycle =
        tsToDate(cetus.Expiry.$date.$numberLong) - new Date();

    // if more than 50min then timeleft until last 50min (aka night)
    let msIn50mins = 50 * 60 * 1000;
    let timeLeft = (timeLeftUntilNewCycle - msIn50mins);

    if (timeLeft < 0) timeLeft = timeLeftUntilNewCycle;

    timeLeft = msToSecMinHourDay(timeLeft);
    timeLeftUntilNewCycle = msToSecMinHourDay(timeLeftUntilNewCycle);

    if (timeLeftUntilNewCycle.hours === 0 && timeLeftUntilNewCycle.minutes < 50
    /* &&
      timeLeftUntilNewCycle.minutes >= 5*/) {
        $(selector.cetuspoedaynightcycle).append('Night');
        $(selector.cetuspoedaynightcycle).css('color', 'darkblue');
    } else {
        $(selector.cetuspoedaynightcycle).append('Day');
        $(selector.cetuspoedaynightcycle).css('color', 'orange');
    }

    $(selector.cetuspoetime)
        .append(`Time left: ${
            timeLeft.hours + 'h ' + timeLeft.minutes + 'm ' + timeLeft.seconds +
            's'}`);
}

const processData = function () {
    updateNews();

    updateVoidFissures();

    updateInvasions();

    updateDarvoDeals();

    updateVoidTrader();

    updateAlerts();

    updateSorties();

    updateCetusPOE();
}

const main = function () {
    let request = makeRequest('GET', apiPath + '?api=worldState');

    request.onload = function () {
        console.log('worldState downloaded!');
        currentWorldState = JSON.parse(request.responseText);

        processData();
    };

    request.send();
};


$().ready(() => {
    // initialize the node map
    let nodeMapRequest = makeRequest('GET', apiPath + '?api=nodemap', false);
    // initialize the mission type map
    let mtRequest = makeRequest('GET', apiPath + '?api=missiontype', false);

    nodeMapRequest.send();
    mtRequest.send();

    if (mtRequest.status === 200 && nodeMapRequest.status === 200) {
        missionsTypeMap = JSON.parse(mtRequest.responseText);
        nodeMap = JSON.parse(nodeMapRequest.responseText);
    } else {
        return;
    }

    setInterval(main, tickRateMs);
    main();
});
