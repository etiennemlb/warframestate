<?php


//stream_context_set_default(['http'=>['proxy'=>'proxy:port']]);

if (isset($_GET['api']) && $_GET['api'] != '') {
    if ($_GET['api'] == 'nodemap') {
        echo file_get_contents('./data/node_planets_map.json');
    }

    if ($_GET['api'] == 'missiontype') {
        echo file_get_contents('./data/missions_type_map.json');
    }

    if ($_GET['api'] == 'worldState') {
        echo file_get_contents("http://content.warframe.com/dynamic/worldState.php");
    }

} else {
    echo file_get_contents('./index.html');
}

?>